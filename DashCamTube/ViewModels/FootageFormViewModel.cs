﻿using DashCamTube.Models;
using System.ComponentModel.DataAnnotations;

namespace DashCamTube.ViewModels
{
    public class FootageFormViewModel
    {
        public int Id { get; set; }

        public string UserId { get; set; }

        [Required]
        [StringLength(54)]
        public string Title { get; set; }

        [Required]
        [StringLength(735)]
        public string Description { get; set; }

        [Required]
        [Display(Name = "YouTube link")]
        [RegularExpression(@"http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\]*)(&(amp;)?‌​[\w\?‌​=]*)?", ErrorMessage = "YouTube link should has a format like this: https://www.youtube.com/watch?v=XXXXXXX")]
        public string YtLink { get; set; }

        [StringLength(9)]
        [Display(Name = "Registration Number")]
        public string RegistrationNumber { get; set; }

        public ApplicationUser User { get; set; }

        public bool IsAlertInfo { get; set; } = false;

        public string AlertInfo { get; set; }

        //Constructors

        public FootageFormViewModel()
        {
        }

        public FootageFormViewModel(string alertInfo)
        {
            IsAlertInfo = true;
            AlertInfo = alertInfo;
        }

        public string GetEmbedLink()
        {
            if (YtLink.Contains("watch?v="))
            {
                string link = YtLink.Replace("watch?v=", "embed/");
                return link;
            }

            return YtLink;
        }

        public string GetFromattedRegNumber()
        {
            if (RegistrationNumber.Contains(" "))
                return RegistrationNumber;

            //string toReturn;
            int resultNum;
            string toReturn;
            for (int i = 0; i < RegistrationNumber.Length; i++)
            {
                bool isNum = int.TryParse(RegistrationNumber[i].ToString(), out resultNum);

                if (isNum && i >= 2)
                {
                    //ex. WE + " " + 46353
                    string leftPart = RegistrationNumber.Substring(0, i);
                    string rightpart = RegistrationNumber.Substring(i, RegistrationNumber.Length - i);
                    //toReturn = RegistrationNumber.Substring(0, i) + " " + RegistrationNumber.Substring(i + 2, RegistrationNumber.Length - i);
                    return leftPart + " " + rightpart;
                }

            }

            return RegistrationNumber;
        }
    }
}