﻿using DashCamTube.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace DashCamTube.ViewModels
{
    //ViewModel which is passed to Single View of Footage (contains Footage and comments)
    public class SingleFootageViewModel
    {
        //Footage properties
        public Footage Footage { get; set; }



        //Comment properties
        public string UserCommentId { get; set; }

        public int FootageId { get; set; }

        [MinLength(5, ErrorMessage = "The comment length must be between 5 and 700 characters")]
        [Display(Name = "Comment")]
        [Required(ErrorMessage = "The comment length must be between 5 and 700 characters")]
        [StringLength(700, ErrorMessage = "The comment length must be between 5 and 700 characters")]
        public string Content { get; set; }

        public DateTime DateTime { get; set; }

        public bool IsCommentFormValid { get; set; }

        public string CommentInfo { get; set; }

        //public string TextInfo { get; set; }

    }
}