﻿using DashCamTube.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace DashCamTube.ViewModels
{
    public class CommentViewModel
    {
        public string UserId { get; set; }
        public int FootageId { get; set; }

        public Footage Footage { get; set; }

        [MinLength(5)]
        [Required]
        [StringLength(700)]
        [Display(Name = "Comment")]
        public string Content { get; set; }

        public DateTime DateTime { get; set; }

        //public bool SendsSucceed { get; set; }

        public CommentViewModel(string userId, int footageId)
        {
            UserId = userId;
            FootageId = footageId;
        }

        public CommentViewModel()
        {
        }
    }
}