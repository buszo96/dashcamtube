﻿using DashCamTube.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace DashCamTube.ViewModels
{
    public class UserNameValidation : ValidationAttribute
    {
        private ApplicationDbContext _context { get; set; }

        public UserNameValidation()
        {
            _context = new ApplicationDbContext(); ;
        }
        public override bool IsValid(object value)
        {
            string userName = Convert.ToString(value);
            if (_context.Users.Any(u => u.UserName == userName))
                return false;

            return true;
        }
    }
}