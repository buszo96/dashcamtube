﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DashCamTube.Models
{
    public class Comment
    {
        public int Id { get; set; }

        [Required]
        public string UserId { get; set; }

        [Required]
        public int FootageId { get; set; }


        [StringLength(700, MinimumLength = 5)]
        public string Content { get; set; }

        public DateTime DateTime { get; set; }



        //nav props
        public ApplicationUser User { get; set; }
        public Footage Footage { get; set; }
    }
}
