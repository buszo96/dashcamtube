﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DashCamTube.Models
{
    public class Footage
    {
        public int Id { get; set; }

        public string UserId { get; set; }

        [Required]
        [StringLength(54)]
        public string Title { get; set; }

        [StringLength(735)]
        public string Description { get; set; }

        public string YtLink { get; set; }

        [StringLength(9)]
        public string RegistrationNumber { get; set; }

        public ApplicationUser User { get; set; }

        [Required]
        public DateTime Time { get; set; }

        public List<Comment> Comments { get; set; }
    }
}