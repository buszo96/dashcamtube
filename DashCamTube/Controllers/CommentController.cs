﻿using DashCamTube.Models;
using DashCamTube.ViewModels;
using System;
using System.Web.Mvc;

namespace DashCamTube.Controllers
{
    [Authorize]
    public class CommentController : Controller
    {
        private ApplicationDbContext _context { get; set; }


        public CommentController()
        {
            _context = new ApplicationDbContext();
        }


        //[HttpPost]
        public ActionResult AddComment(CommentViewModel commentViewModel)
        {

            if (!ModelState.IsValid)
                return RedirectToAction("SingleViewOfFootage", "Footage", new { id = commentViewModel.FootageId });
            //return PartialView("_CommentForm", commentViewModel);


            var comment = new Comment()
            {
                Content = commentViewModel.Content,
                DateTime = DateTime.Now,
                FootageId = commentViewModel.FootageId,
                UserId = commentViewModel.UserId
            };

            _context.Comments.Add(comment);
            _context.SaveChanges();
            //return PartialView("_CommentForm", commentViewModel);

            return RedirectToAction("SingleViewOfFootage", "Footage", new { id = commentViewModel.FootageId });

            //return PartialView("_CommentForm", new CommentViewModel(commentViewModel.UserId, commentViewModel.FootageId));
        }
    }
}