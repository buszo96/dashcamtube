﻿using DashCamTube.Models;
using Microsoft.AspNet.Identity;
using System.Web.Http;

namespace DashCamTube.Controllers.Api
{
    [Authorize]
    public class CommentController : ApiController
    {
        private ApplicationDbContext _context { get; set; }

        public CommentController()
        {
            _context = new ApplicationDbContext();
        }

        [HttpPost]
        public IHttpActionResult Add([FromBody]Comment comment)
        {
            string userId = User.Identity.GetUserId();

            if (comment == null)
                return BadRequest();

            _context.Comments.Add(comment);
            _context.SaveChanges();
            return Ok();
        }
    }
}
