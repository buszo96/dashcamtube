﻿using DashCamTube.Models;
using PagedList;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;


namespace DashCamTube.Controllers
{
    public class HomeController : Controller
    {

        private ApplicationDbContext _context { get; set; }

        public HomeController()
        {
            _context = new ApplicationDbContext();
        }


        //[Route("Page/{page?}")]
        public ActionResult Index(int? page)
        {
            var footages = _context.Footages
                .Include(f => f.User)
                .OrderByDescending(f => f.Time)
                .ToList().ToPagedList(page ?? 1, 10);

            return View(footages);
        }











        //public ActionResult About()
        //{
        //    ViewBag.Message = "Your application description page.";

        //    return SingleViewOfFootage();
        //}

        //public ActionResult Contact()
        //{
        //    ViewBag.Message = "Your contact page.";

        //    return SingleViewOfFootage();
        //}
    }
}