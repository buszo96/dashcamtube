﻿using DashCamTube.Models;
using DashCamTube.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace DashCamTube.Controllers
{
    public class FootageController : Controller
    {
        private ApplicationDbContext _context;

        public FootageController()
        {
            _context = new ApplicationDbContext();
        }

        [Authorize]
        [HttpGet]
        public ActionResult AddFootage()
        {
            return View(new FootageFormViewModel());
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddFootage(FootageFormViewModel footageFromForm)
        {
            if (!ModelState.IsValid)
                return View(footageFromForm);

            var footage = new Footage()
            {
                UserId = User.Identity.GetUserId(),
                Title = footageFromForm.Title,
                Description = footageFromForm.Description,
                YtLink = footageFromForm.GetEmbedLink(),
                RegistrationNumber = footageFromForm.GetFromattedRegNumber(),
                Time = DateTime.Now
            };

            _context.Footages.Add(footage);
            _context.SaveChanges();


            return View(new FootageFormViewModel("You successfully added your footage!"));
        }

        //[Route("footage/{id?}")]
        public ActionResult SingleViewOfFootage(int id)
        {
            var footage = _context.Footages
                .Include(f => f.User)
                .Include(f => f.Comments)
                .SingleOrDefault(f => f.Id == id);

            if (footage == null)
                return RedirectToAction("Index", "Home");

            return View(footage);
        }

    }
}