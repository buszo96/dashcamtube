﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DashCamTube.Startup))]
namespace DashCamTube
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
