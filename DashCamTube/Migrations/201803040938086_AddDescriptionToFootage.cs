namespace DashCamTube.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDescriptionToFootage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Footages", "Description", c => c.String(maxLength: 325));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Footages", "Description");
        }
    }
}
