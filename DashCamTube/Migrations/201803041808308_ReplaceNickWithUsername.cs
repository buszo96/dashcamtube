namespace DashCamTube.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReplaceNickWithUsername : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AspNetUsers", "Nick");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "Nick", c => c.String(nullable: false, maxLength: 15));
        }
    }
}
