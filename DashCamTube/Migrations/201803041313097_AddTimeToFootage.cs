namespace DashCamTube.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTimeToFootage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Footages", "Time", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Footages", "Time");
        }
    }
}
