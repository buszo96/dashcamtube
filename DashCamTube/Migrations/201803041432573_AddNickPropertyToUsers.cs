namespace DashCamTube.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNickPropertyToUsers : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Nick", c => c.String(nullable: false, maxLength: 15));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "Nick");
        }
    }
}
