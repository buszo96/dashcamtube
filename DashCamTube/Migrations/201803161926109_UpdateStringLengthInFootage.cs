namespace DashCamTube.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateStringLengthInFootage : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Footages", "Title", c => c.String(nullable: false, maxLength: 54));
            AlterColumn("dbo.Footages", "Description", c => c.String(maxLength: 735));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Footages", "Description", c => c.String(maxLength: 325));
            AlterColumn("dbo.Footages", "Title", c => c.String(nullable: false, maxLength: 255));
        }
    }
}
