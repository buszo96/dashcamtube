namespace DashCamTube.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCommentModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        FootageId = c.Int(nullable: false),
                        Content = c.String(maxLength: 700),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Footages", t => t.FootageId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.FootageId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Comments", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Comments", "FootageId", "dbo.Footages");
            DropIndex("dbo.Comments", new[] { "FootageId" });
            DropIndex("dbo.Comments", new[] { "UserId" });
            DropTable("dbo.Comments");
        }
    }
}
